
# coding: utf-8

# In[57]:


import pandas as pd
import numpy as np


from keras.models import Model
from keras.layers import Dense, Input, Dropout, LSTM, Activation
from keras.utils import np_utils


# In[119]:


df = pd.read_csv("./data/data_next_60_min.csv")
df = df.drop(columns=['Unnamed: 0'])

df = df.drop(df[df.isnull().values].index, axis = 0)

for i in range(1,12):
    df["type_id_"+str(i)] = df["type_id_"+str(i)].astype('category')
    df["team_id_"+str(i)] = df["team_id_"+str(i)].astype('category')
    
for i in range(1,12):
    df["x_"+str(i)] = (df["x_"+str(i)] + 2)/104
    df["y_"+str(i)] = (df["y_"+str(i)] + 2)/104

df['min'] = df['min']/90

df = df.sample(frac=1)
X = df.drop(columns=['team_id_11', 'type_id_11', 'x_11', 'y_11'])
Y = df[['team_id_11', 'x_11', 'y_11', 'type_id_11']]

n_exp, p = X.shape


# In[121]:



X_arr = np.array(X.drop(columns=['min']))


n_ev = 10
p_ev = p/n_ev


X_arr = X_arr.reshape((n_exp, p_ev, n_ev))

X_arr = np.transpose(X_arr, (0,2,1))


x_min_rep = np.repeat(np.array(X['min']), n_ev).reshape(n_exp, n_ev)
X_arr = np.insert(X_arr, 0, x_min_rep, axis = 2)


X_cat = np.delete(X_arr, 2, 2)
x_type_cat =  np_utils.to_categorical(X_arr[:,:,2])
X_arr = np.insert(X_cat, [0]*x_type_cat.shape[2], x_type_cat, axis = 2)


print X_arr.shape
print X_arr[0,:,-5:-1]
_, _, p_ev = X_arr.shape


# In[122]:


Y_team = np_utils.to_categorical(Y['team_id_11'])
print Y_team.shape
print Y_team[:5,:]


# In[129]:


Y_pos = np.array(Y[['x_11', 'y_11']])
print Y_pos.shape


# In[138]:


n_train = int(0.75*n_exp)


X_train = X_arr[:n_train]
Y_train = {
    "team_output":Y_team[:n_train,:],        
    "pos_output":Y_pos[:n_train,:]
}

X_test = X_arr[n_train:]
Y_test = {
    "team_output":Y_team[n_train:,:],        
    "pos_output":Y_pos[n_train:,:]
}


# In[147]:


def lstm_model(n_layer, n_units, input_shape, do = 0.5):
    input_mat = Input(shape=input_shape)
    
    X = LSTM(n_units[0], return_sequences=True)(input_mat)
    X = Dropout(do)(X)
    
    for l in range(1,n_layer-1):
        X = LSTM(n_units[l], return_sequences=True)(X)
        X = Dropout(do)(X)
        
    X = LSTM(n_units[-1])(X)
    X = Dropout(do)(X)
    
    X_team = Dense(2, activation='softmax', name = "team_output")(X)
    X_pos  = Dense(2, activation='sigmoid', name = "pos_output")(X)
    model = Model(input_mat, [X_team, X_pos])
    
    return model


# In[148]:


hu = [200, 200]
n_layer = len(hu)
model = lstm_model(n_layer, hu, (n_ev, p_ev,))
model.summary()


# In[149]:


losses = {
    "team_output": "categorical_crossentropy",
    "pos_output": "mean_squared_error",
}

lossWeights = {"team_output": 1.0, "pos_output": 1.0}
metrics = {    
    "team_output": "accuracy",
    "pos_output": "mse"
}

model.compile(loss=losses,loss_weights=lossWeights, optimizer='adam', metrics=metrics)


# In[150]:


hist = model.fit(X_train, Y_train, epochs = 50, batch_size = 32, shuffle=True)


# In[152]:


eva = model.evaluate(X_test, Y_test)
print acc


# In[162]:


pred = model.predict(X_test)
pred_team = np.argmax(pred[0], axis = 1)
pred_x = pred[1][:,0]
pred_y = pred[1][:,1]

acc = np.sum(np.argmax(Y_test["team_output"],axis=1) == pred_team)/float(pred_team.shape[0])
rm_x = np.sqrt(sum((pred_x-Y_test["pos_output"][:,0])**2)/float(pred_x.shape[0]))
rm_y = np.sqrt(sum((pred_y-Y_test["pos_output"][:,1])**2)/float(pred_y.shape[0]))
print('ACCURACY_TEAM',acc)
print('RMSE_X = ',rm_x)
print('RMSE_y = ',rm_y)


# In[163]:


# Save the weights
mod_name = "./model/model_ntp_lstm_"+str(n_layer)+"_"+str(hu)+"_acc_"+str(acc)[:4]+"_rmx_"+str(rm_x)[:4]+"_rmy_"+str(rm_y)[:4]
model.save_weights(mod_name+"_weights.h5")
# Save the model architecture
with open(mod_name+"_architecture.json", 'w') as f:
    f.write(model.to_json())


# In[164]:


from keras.models import model_from_json

# Model reconstruction from JSON file
with open("./model/model_ntp_lstm_2_[200, 200]_acc_0.84rmx_0.19rmy_0.20_architecture.json", 'r') as f:
    model = model_from_json(f.read())

# Load weights into the new model
model.load_weights("model/model_ntp_lstm_2_[200, 200]_acc_0.84rmx_0.19rmy_0.20_weights.h5")


# In[165]:


model.summary()


# In[166]:


pred = model.predict(X_test)


# In[167]:


print pred[0][:10,]


# In[84]:


print np.argmax(pred,axis=1)[:10]
print np.argmax(Y_test,axis=1)[:10]

