
# coding: utf-8

# In[141]:


import xml.etree.ElementTree as ET
import pandas as pd
import numpy as np

import os

N = 131

#transforme les données d'un match en xml en csv. 
# - récupère les informations présentent dans un fichier après modification
# - sélection des qualifiers (voir doc taha)
def xml_to_csv(xml_file):
    tree1 = ET.parse(xml_file)
    root1 = tree1.getroot()
    
    event_keys1 = root1[0][10].attrib.keys()
    toremove = ['version', 'timestamp','last_modified']
    for tr in toremove:
        if tr in event_keys1:
            event_keys1.remove(tr)


    listeQ = [22,23,24,25,26,15,
          72,20,21,160,154,108,
          113,117,120,122,133,9,
          28,121,136,137,138,153,
          214,215,217,228,249,250,
          252,253,254]
    
    listeQ = [str(i) for i in listeQ]
    
    dict_ex1 = {}
    
    for k in event_keys1 + listeQ:
        dict_ex1[k] = []
        
        
    for e in root1[0]:
        for qi in listeQ:
            dict_ex1[qi].append("")
            for q in e:
                q_id = q.attrib["qualifier_id"]
                if q_id == qi:
                    if "value" in q.attrib:
                        dict_ex1[q_id][-1] = q.attrib["value"]
                    else:
                        dict_ex1[q_id][-1] = 1

        for k in event_keys1:
            if k in e.attrib:
                dict_ex1[k].append(e.attrib[k])
            else:
                dict_ex1[k].append("")
                
    df_ex = pd.DataFrame(dict_ex1)
    
    return df_ex, event_keys1, listeQ



def ex_to_vec(df_ex, event_keys1, listeQ, count_team = True):
    # On compte le nombre de fois qu'apparaissent un type d'évenement
    df_ex = pd.DataFrame(df_ex).iloc[:-10,]

    keep_event = [1,2,3,4,5,6,7,8,9, 
                10,11,12,13,14,15,16,17,24,
                30,32,34,35,37,38,39,40,41,
                42,44,45,49,50,51,52,53,54,
                55,56,59,60,61,63,66,72,74] #Liste de type d'évenement à garder

    keep_event = [ str(i) for i in keep_event]

    keys_to_keep = event_keys1

    count = {}
    dict_player = {}
    
    tid = df_ex["team_id"].unique()
    
    for col in event_keys1:
        
        if col == 'type_id':
            for ev in keep_event:
                idx1 =  (df_ex['outcome'] == "1")&(df_ex[col] == ev)
                idx0 =  (df_ex['outcome'] == "0")&(df_ex[col] == ev)

                count[ev + "_out_1"] = [np.sum(idx1)]
                count[ev + "_out_0"] = [np.sum(idx0)]
            
        elif col == "team_id":
            
            
            if count_team == True:
                idx0 = (df_ex[col] == tid[0])
                idx1 = (df_ex[col] == tid[1])
                
                count['team_id_0'] = [np.sum(idx0)]
                count['team_id_1'] = [np.sum(idx1)]
            else:
                count['team_id_0'] = [-1]
                count['team_id_1'] = [-1]
            
        elif col == "min":
            count[col] = pd.to_numeric(df_ex["min"]).mean()
        
        
        
    df_count = pd.DataFrame(count)

    for q in listeQ:
        tmp = df_ex[q].replace("", -1)
        if np.sum(tmp != -1) != 0:
            df_count["mean_q_"+q] = pd.to_numeric(tmp[tmp != -1]).mean()
        else:
            df_count["mean_q_"+q] = -1
    
    df_count["game_id"] = [df_ex["id"]]
    
    return df_count


# In[159]:


def extract_player_data(df,event_keys1 ,listeQ ):
    
    liste_player = df["player_id"].unique()
    if '' in liste_player:
        liste_player = np.delete(liste_player, liste_player == '')
    
    df_player = pd.DataFrame(columns = ["" for i in range(N  - 3)])
    e = 0
    for pl in liste_player:
        idx = df["player_id"] == pl
        df_red = df[idx]
        df_red = ex_to_vec(df_red, event_keys1, listeQ, False)
        df_red["player_id"] = pl
        if e == 0:
            df_player.columns = df_red.columns
            df_player.loc[e] = df_red.loc[0]

        else:
            df_player.loc[e] = df_red.loc[0] 
        e += 1
        
    return df_player


# In[160]:


def get_game_info(f):
    tree1 = ET.parse(f)
    root1 = tree1.getroot()
    
    
    atn = root1[0].attrib["away_team_name"]
    htn = root1[0].attrib["home_team_name"]
    
    ati = root1[0].attrib["away_team_id"]
    hti = root1[0].attrib["home_team_id"]
    
    return [atn, htn, ati, hti]


# In[167]:


def create_trains(n_ex_game):
    arr = os.listdir('./train_xml/')
    
    df_ex_equipe = pd.DataFrame(columns = ["" for i in range(N)])
    df_ex_player = pd.DataFrame(columns = ["" for i in range(N+1)])
    e = 0
    p = 0
    
    team_name = []
    print "games : ", len(arr)
    for fn in arr:
        if e%100 == 0:
            print e," / ",len(arr)*n_ex_game
        f = './train_xml/' + fn
        dt, ek, lq = xml_to_csv(f)
        
        
        tn = get_game_info(f)
        for i in range(n_ex_game):
            per = np.random.randint(1)
            t = np.random.randint(30)*per + (45 + np.random.randint(30))*(1-per)
            
            dt_red = dt[(pd.to_numeric(dt["min"])>=t)*(pd.to_numeric(dt["min"]) <= t+15) ]
            
            count = ex_to_vec(dt_red, ek, lq)
            count["away_team_name"] = [tn[0]]
            count["home_team_name"] = [tn[1]]
            count["away_team_id"] = [tn[2]]
            count["home_team_id"] = [tn[3]]
            if e == 0:

                df_ex_equipe.columns = count.columns
                df_ex_equipe.loc[e] = count.loc[e]
            else:

                # df_ex_equipe.append(count, ignore_index=True)
                #pd.concat([df_ex_equipe, count])
                df_ex_equipe.loc[e] = count.loc[0]  # adding a row

            
            df_play = extract_player_data(dt_red, ek, lq)
            df_play["away_team_name"] = [tn[0]]*df_play.shape[0]
            df_play["home_team_name"] = [tn[1]]*df_play.shape[0]
            df_play["away_team_id"] = [tn[2]]*df_play.shape[0]
            df_play["home_team_id"] = [tn[3]]*df_play.shape[0]

            for i in range(df_play.shape[0]):
                if p == 0:
                    df_ex_player.columns = df_play.columns
                    df_ex_player.loc[p] = df_play.loc[i]
                else:
                    df_ex_player.loc[p] = df_play.loc[i]
                    
                p += 1
            e += 1
    return df_ex_equipe, df_ex_player


# In[168]:


import time


# In[169]:


t = time.time()
df_eq, df_pl = create_trains(10)
print "time : ", (time.time() - t)/60.
df.to_csv("train_equipe_10.csv")


# In[ ]:


t = time.time()
df_eq, df_pl = create_trains(50)
print "time : ", (time.time() - t)/60.
df.to_csv("train_equipe_10.csv")

