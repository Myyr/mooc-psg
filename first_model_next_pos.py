
import pandas as pd
import numpy as np
import sklearn.neural_network as sk
import sklearn.ensemble as ske
import sklearn.linear_model as skl
# In[67]:


df = pd.read_csv("./data/pred_pos_team_15.csv")


# In[68]:


print df.columns
print df.shape
df.head()


# In[69]:


team_ids = []
team_id_col = []
for i in range(1,11):
    team_ids += df["team_id_" + str(i)].unique().tolist()
    team_id_col += ["team_id_" + str(i)]
team_ids = np.unique(np.array(team_ids))


# In[70]:


print team_ids


# In[71]:


df = df.drop(columns=['Unnamed: 0'])


# In[76]:


df.head()


# In[80]:


df.isnull().values


# In[81]:


df = df.drop(df[df.isnull().values].index, axis = 0)
print df.isnull().values.any()


# In[82]:



for i in range(1,11):
    
    df["team_id_" + str(i)] .unique().tolist()
team_ids = np.unique(np.array(team_ids))


# In[83]:


df = df.sample(frac=1)
df['team_id_1'] = df['team_id_1'].astype('category')
df['team_id_2'] = df['team_id_2'].astype('category')
df['team_id_3'] = df['team_id_3'].astype('category')
df['team_id_4'] = df['team_id_4'].astype('category')
df['team_id_5'] = df['team_id_5'].astype('category')
df['team_id_6'] = df['team_id_6'].astype('category')
df['team_id_7'] = df['team_id_7'].astype('category')
df['team_id_8'] = df['team_id_8'].astype('category')
df['team_id_9'] = df['team_id_9'].astype('category')
df['team_id_10'] = df['team_id_10'].astype('category')
df['team_id_11'] = df['team_id_11'].astype('category')
df['type_id_1'] = df['type_id_1'].astype('category')
df['type_id_2'] = df['type_id_2'].astype('category')
df['type_id_3'] = df['type_id_3'].astype('category')
df['type_id_4'] = df['type_id_4'].astype('category')
df['type_id_5'] = df['type_id_5'].astype('category')
df['type_id_6'] = df['type_id_6'].astype('category')
df['type_id_7'] = df['type_id_7'].astype('category')
df['type_id_8'] = df['type_id_8'].astype('category')
df['type_id_9'] = df['type_id_9'].astype('category')
df['type_id_10'] = df['type_id_10'].astype('category')
df['type_id_11'] = df['type_id_11'].astype('category')

X = df.drop(columns=['team_id_11', 'type_id_11', 'x_11', 'y_11'])

Y = df[['team_id_11', 'x_11', 'y_11']]


# In[84]:


print X.shape
print Y.shape

n_exp, p = X.shape


# In[85]:


n_train = int(0.75*n_exp)


X_train = X[:n_train]
Y_train = Y[:n_train]

X_test = X[n_train:]
Y_test = Y[n_train:]


# In[86]:


from sklearn.neural_network import MLPClassifier


# In[100]:


hl_size = 200
hl_nb = 200
clf = sk.MLPRegressor(activation='relu',
                        solver='adam',
                        learning_rate='adaptive',
                        max_iter=1000,
                        learning_rate_init=0.01,
                        hidden_layer_sizes=(hl_size, hl_nb), 
                        random_state=1)


# In[101]:


clf.fit(X_train, Y_train["x_11"]) 


# In[102]:


Y_pred = clf.predict(X_test)


# In[103]:

print('RMSE_X = ',np.sqrt(sum((Y_pred-Y_test["x_11"])**2)/float(Y_pred.shape[0])))
print('RMSNULX = ',np.sqrt(sum((np.mean(Y_test["x_11"])-Y_test["x_11"])**2)/float(Y_pred.shape[0])))
clf.fit(X_train, Y_train["y_11"]) 
Y_pred = clf.predict(X_test)
print('RMSE_Y = ',np.sqrt(sum((Y_pred-Y_test["y_11"])**2)/float(Y_pred.shape[0])))
print('RMSNULY = ',np.sqrt(sum((np.mean(Y_test["y_11"])-Y_test["y_11"])**2)/float(Y_pred.shape[0])))
# In[104]:

regr = ske.RandomForestRegressor(max_depth=35, random_state=0,
                               n_estimators=500)
regr.fit(X_train, Y_train["x_11"])  
Y_pred = clf.predict(X_test)                             
print('RMSE_X (randomforest) = ',np.sqrt(sum((Y_pred-Y_test["x_11"])**2)/float(Y_pred.shape[0])))

regr = skl.LinearRegression().fit(X_train, Y_train["x_11"])
Y_pred = regr.predict(X_test)                             
print('RMSE_X (linear) = ',np.sqrt(sum((Y_pred-Y_test["x_11"])**2)/float(Y_pred.shape[0])))