import pandas as pd 
import xml.etree.ElementTree as ET
import numpy as np
#keep the final 10 events of the csv 
def last_event( df ):
    return df[-11:]

def lasttestevent(xml_file):
    tree1 = ET.parse(xml_file)
    root1 = tree1.getroot()
    
    event_keys1 = root1[0][10].attrib.keys()
    toremove = ['version', 'timestamp','last_modified', 'id']
    for tr in toremove:
        if tr in event_keys1:
            event_keys1.remove(tr)

    dict_ex1 = {}
    for k in event_keys1 :
        dict_ex1[k] = []

    for e in root1[0]:       
        for k in event_keys1:
            if k in e.attrib:
                dict_ex1[k].append(e.attrib[k])
            else:
                dict_ex1[k].append("")



#recupère les 11 derniers events+infos de l'XML 
    df_ex = last_event(pd.DataFrame(dict_ex1))
    df_ex["team_id"]= (df_ex["home_team_id"]==df_ex["team_id"])
    df_ex = df_ex.transpose()
    return (df_ex)



def array10ev(df):
    arr = np.array(df)
    line = np.append(np.append(np.append(
        np.append(arr[1,0],arr[6,:]),arr[7,:]),arr[8,:]),arr[9,:])
    return line


def newtrain():
     return pd.DataFrame( columns=["min"]+[ i  + str(j) for i in ["team_id_", "type_id_", "x_","y_" ] for j in range(1,12)])


test = array10ev(lasttestevent("exemlpe.xml"))
new = newtrain()
new.loc[0]=test
print(new)


    





