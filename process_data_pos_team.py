
# coding: utf-8

# In[17]:


import pandas as pd 
import xml.etree.ElementTree as ET
import numpy as np

import os


# In[49]:




def last_event( df ):
    return df[-11:]

def lasttestevent(xml_file):
    tree1 = ET.parse(xml_file)
    root1 = tree1.getroot()
    
    event_keys1 = root1[0][10].attrib.keys()
    toremove = ['version', 'timestamp','last_modified', 'id']
    for tr in toremove:
        if tr in event_keys1:
            event_keys1.remove(tr)
    
    dict_ex1 = {}
    for k in event_keys1 :
        dict_ex1[k] = []

    for e in root1[0]:       
        for k in event_keys1:
            if k in e.attrib:
                dict_ex1[k].append(e.attrib[k])
            else:
                dict_ex1[k].append("")

#recupère les 11 derniers events+infos de l'XML 
    dt = pd.DataFrame(dict_ex1)
    
    per = np.random.randint(1)
    t = np.random.randint(30)*per + (45 + np.random.randint(30))*(1-per)
    dt_red = dt[(pd.to_numeric(dt["min"])>=t)*(pd.to_numeric(dt["min"]) <= t+15) ]

    df_ex = last_event(dt_red)
    hti = root1[0].attrib["home_team_id"]
    df_ex["team_id"]= (hti==df_ex["team_id"]).astype(int)
    print(df_ex)
    df_ex = df_ex.transpose()
    
    new = newtrain()
    
    df_ex = array10ev(df_ex)
    
    new.loc[0]= df_ex
    
    return new

def array10ev(df):
    arr = np.array(df)
    line = np.append(np.append(np.append(
        np.append(arr[1,0],arr[6,:]),arr[7,:]),arr[8,:]),arr[9,:])
    return line


def newtrain():
     return pd.DataFrame( columns=["min"]+[ i  + str(j) for i in ["team_id_", "type_id_", "x_","y_" ] for j in range(1,12)])


# In[56]:


def create_trains(n_ex_game):
    arr = os.listdir('./train_xml/')
    
    df_ex_equipe = pd.DataFrame(columns = ["" for i in range(45)])
    
    e = 0
    

    print "games : ", len(arr)
    for fn in arr:
        if e%100 == 0:
            print e," / ",len(arr)*n_ex_game
        f = './train_xml/' + fn
        

        for i in range(n_ex_game):
            
            
            count = lasttestevent(f)

            if e == 0:

                df_ex_equipe.columns = count.columns
                df_ex_equipe.loc[e] = count.loc[e]
            else:
                df_ex_equipe.loc[e] = count.loc[0]
    

            e += 1
    return df_ex_equipe


# In[57]:


dt = create_trains(1)

