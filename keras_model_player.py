
# coding: utf-8

# In[48]:


import pandas as pd
import numpy as np


from keras.models import Model
from keras.layers import Dense, Input, Dropout, LSTM, Activation
from keras.utils import np_utils
from keras.utils import normalize

from sklearn import preprocessing


# In[49]:


df_equipe = pd.read_csv("data/train_equipe_new_10.csv")
df_player = pd.read_csv("data/train_player_new_10.csv")

df_equipe = df_equipe.drop(df_equipe[df_equipe.isnull().values].index, axis = 0)
df_player = df_player.drop(df_player[df_player.isnull().values].index, axis = 0)

tid = [ 139,  140,  143,  144,  145,  146,  147,  148,  149,  150,  152,  427,  428,  429,
  430,  694, 1028, 1395, 2128, 2130]

print df_equipe.isnull().values.any()
print df_player.isnull().values.any()

print df_equipe.shape
print df_player.shape


# In[35]:


df_equipe = df_equipe.sample(frac=1)
X = df_equipe.drop(columns=['Unnamed: 0','game_id', 'team_id', 'team_name'])
Y = df_equipe[['team_id', 'team_name']]

min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(X)
X = pd.DataFrame(x_scaled)

Y['id'] = np.zeros(Y.shape[0])
for i in range(len(tid)):
    Y['id'][Y["team_id"] == tid[i]] = i
    
Y_id = np_utils.to_categorical(Y["id"])

print X.shape
print Y_id.shape

n_exp, p = X.shape
_, n_tid = Y_id.shape


# In[36]:


n_train = int(0.75*n_exp)


X_train = X[:n_train]
X_test = X[n_train:]

Y_train = Y_id[:n_train]
Y_test = Y_id[n_train:]

print X_train.shape
print Y_train.shape
print X_test.shape
print Y_test.shape


# In[5]:


def model_team(n_layer, n_units, input_shape, do = 0.5):
    input_vec = Input(shape=input_shape)
    
    X = Dense(n_units[0])(input_vec)
    X = Dropout(do)(X)
    
    for l in range(1,n_layer-1):
        X = Dense(n_units[l], activation="relu")(X)
        X = Dropout(do)(X)
        
    X = Dense(n_units[-1])(X)
    X = Dropout(do)(X)
    
    X_team = Dense(20, activation='softmax', name = "team_output")(X)
    
    model = Model(input_vec, X_team)
    
    return model


# In[18]:


#n_layer = 10
#hu = [ int(50 + (n_layer - i)*45) for i in range(n_layer)]

hu = [500,200,150,50]
n_layer = len(hu)

print n_layer
print hu[:10]
print hu[-1]
model = model_team(n_layer, hu, (p,))
model.summary()


# In[19]:


model.compile(loss= "categorical_crossentropy", optimizer='adam', metrics=['accuracy'])


# In[20]:


hist = model.fit(X_train, Y_train, epochs = 1000, batch_size = 32, shuffle=True)


# In[21]:


loss, acc = model.evaluate(X_test, Y_test)
print acc


# In[22]:


import plotly.plotly as py
import plotly.graph_objs as go


# In[29]:


trace0 = go.Scatter(
    x=[i for i in range(500)],
    y=hist.history["acc"]
)
trace1 = go.Scatter(
    x=[i for i in range(500)],
    y=hist.history["loss"]
)
data = [trace0, trace1]
py.plot(data)


# In[25]:


# Save the weights
mod_name = "./model/model_eq_"+str(n_layer)+"_"+str(hu)+"_acc_"+str(acc)[:4]
model.save_weights(mod_name+"_weights.h5")
# Save the model architecture
with open(mod_name+"_architecture.json", 'w') as f:
    f.write(model.to_json())


# ------------------------------------------------

# In[71]:


df_player = df_player.sample(frac=1)
Xp = df_player.drop(columns=['Unnamed: 0','game_id', 'player_id', 
                             'player_team', 'away_team_name',
                             'home_team_name', 'away_team_id', 
                             'home_team_id'
                            ])
Yp = df_player[['player_id', 'player_team']]
Yp['team_id'] = np.zeros(Yp.shape[0])
for i in range(len(tid)):
    Yp['team_id'][Yp["player_team"] == tid[i]] = i
    
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(Xp)
Xp = pd.DataFrame(x_scaled)


print Xp.shape
print Yp.shape

n_exp, p = Xp.shape


# In[87]:


def model_player(n_layer, n_units, input_shape, n_player , do = 0.5):
    input_vec = Input(shape=(input_shape,))
    
    X = Dense(n_units[0])(input_vec)
    X = Dropout(do)(X)
    
    for l in range(1,n_layer-1):
        X = Dense(n_units[l], activation="relu")(X)
        X = Dropout(do)(X)
        
    X = Dense(n_units[-1])(X)
    X = Dropout(do)(X)
    
    X_team = Dense(n_player, activation='softmax', name = "team_output")(X)
    
    model = Model(input_vec, X_team)
    
    return model


# In[100]:


models_player = {}
players_id = {}
mod_accuracy = {}
mod_hist = {}
for i in range(n_tid):
    print "\nt", str(i),"\n"
    hu = [ 500,300,200 ]
    n_layer = len(hu)
    idx = Yp['team_id'] == i
    
    Xp_red = Xp.values[idx]
    Yp_red = Yp.values[idx]
    
    pid = Yp[idx]['player_id'].unique().tolist()
    
    Yp_red = np.insert(Yp_red,0, np.zeros(Yp_red.shape[0]), axis = 1)
    for j in range(len(pid)):
        Yp_red[:,0][Yp[idx]["player_id"] == pid[j]] = j
    Yp_red = np_utils.to_categorical(Yp_red[:,0])
    npl = Yp_red.shape[1]
    
    
    n_train = int(0.85*Xp_red.shape[0])


    Xp_train = Xp_red[:n_train,:]
    Xp_test = Xp_red[n_train:,:]
    Yp_train = Yp_red[:n_train,:]
    Yp_test = Yp_red[n_train:,:]
    
    players_id["t"+str(i)] = pid
    
    models_player["t"+str(i)] = model_player(n_layer = n_layer, n_units = hu, input_shape = p, n_player = npl, do=0.2)
    
    models_player["t"+str(i)].compile(loss= "categorical_crossentropy", optimizer='adam', metrics=['accuracy'])
    
    hist =models_player["t"+str(i)].fit(Xp_train, Yp_train, epochs = 2000, batch_size = 32, shuffle=True, verbose = 0)
    
    mod_hist["t"+str(i)] = hist
    
    loss, acc = models_player["t"+str(i)].evaluate(Xp_test, Yp_test)
    print acc
    mod_accuracy["t"+str(i)] = acc
    
    # Save the weights
    mod_name = "./model/model_pl_t"+str(i)+"_"+str(n_layer)+"_"+str(hu)+"_acc_"+str(acc)[:4]
    models_player["t"+str(i)].save_weights(mod_name+"_weights.h5")
    # Save the model architecture
    with open(mod_name+"_architecture.json", 'w') as f:
        f.write(models_player["t"+str(i)].to_json())
        
with open("pid.json", 'w') as f:
    f.write(players_id.to_json())
    


# In[ ]:



    

