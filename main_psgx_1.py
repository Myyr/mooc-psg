import pandas as pd 
import xml.etree.ElementTree as ET
import numpy as np

import os
import warnings
warnings.filterwarnings("ignore")

from keras.models import model_from_json
from keras.utils import np_utils

from sklearn import preprocessing

import json



N = 127


def last_event( df ):
    return df[-10:]

def lasttestevent(xml_file):
    root1 = xml_file.getroot()
    
    event_keys1 = root1[0][10].attrib.keys()
    toremove = ['version', 'timestamp','last_modified', 'id']
    for tr in toremove:
        if tr in event_keys1:
            event_keys1.remove(tr)
    
    dict_ex1 = {}
    for k in event_keys1 :
        dict_ex1[k] = []

    for e in root1[0]:       
        for k in event_keys1:
            if k in e.attrib:
                dict_ex1[k].append(e.attrib[k])
            else:
                dict_ex1[k].append("")

    dt = pd.DataFrame(dict_ex1)
    
    per = np.random.randint(1)
    t = np.random.randint(30)*per + (45 + np.random.randint(30))*(1-per)
    dt_red = dt[(pd.to_numeric(dt["min"])>=t)*(pd.to_numeric(dt["min"]) <= t+15) ]

    df_ex = last_event(dt_red)
    hti = root1[0].attrib["home_team_id"]
    df_ex["team_id"]= (hti==df_ex["team_id"]).astype(int)
 
    df_ex = df_ex.transpose()
    
    new = newtrain()
    
    df_ex = array10ev(df_ex)
    
    new.loc[0]= df_ex
    
    return new

def array10ev(df):
    arr = np.array(df)
    line = np.append(np.append(np.append(
        np.append(arr[1,0],arr[6,:]),arr[7,:]),arr[8,:]),arr[9,:])
    return line


def newtrain():
     return pd.DataFrame( columns=["min"]+[ i  + str(j) for i in ["team_id_", "type_id_", "x_","y_" ] for j in range(1,11)])


def data_processing_pred_next(df):
    #df = df.drop(columns=['Unnamed: 0'])

    df = df.drop(df[df.isnull().values].index, axis = 0)

    for i in range(1,11):
        df["type_id_"+str(i)] = df["type_id_"+str(i)].astype('category')
        df["team_id_"+str(i)] = df["team_id_"+str(i)].astype('category')
        
    for i in range(1,11):
        df["x_"+str(i)] = (float(df["x_"+str(i)]) + 2.)/104.
        df["y_"+str(i)] = (float(df["y_"+str(i)] )+ 2.)/104.

    df['min'] = float(df['min'])/90

    X = df

    n_exp, p = X.shape

    X_arr = np.array(X.drop(columns=['min']))


    n_ev = 10
    p_ev = p/n_ev


    X_arr = X_arr.reshape((n_exp, p_ev, n_ev))

    X_arr = np.transpose(X_arr, (0,2,1))


    x_min_rep = np.repeat(np.array(X['min']), n_ev).reshape(n_exp, n_ev)
    X_arr = np.insert(X_arr, 0, x_min_rep, axis = 2)


    X_cat = np.delete(X_arr, 2, 2)
    x_type_cat =  np_utils.to_categorical(X_arr[:,:,2], num_classes=75)
    X_arr = np.insert(X_cat, [0]*x_type_cat.shape[2], x_type_cat, axis = 2)

    _, _, p_ev = X_arr.shape

    return X_arr

def predict_next(df):
    with open("./model/model_ntp_lstm_2_[200, 200]_acc_0.84rmx_0.19rmy_0.20_architecture.json", 'r') as f:
        model = model_from_json(f.read())

    model.load_weights("model/model_ntp_lstm_2_[200, 200]_acc_0.84rmx_0.19rmy_0.20_weights.h5")
    X = data_processing_pred_next(df)
    pred = model.predict(X)
    pred_team = np.argmax(pred[0])
    pred_x = pred[1][0,0]*104 - 2
    pred_y = pred[1][0,1]*104 - 2

    return pred_team, pred_x, pred_y

def xml_to_csv(xml_file):
    root1 = xml_file.getroot()
    
    event_keys1 = root1[0][10].attrib.keys()
    toremove = ['version', 'timestamp','last_modified']
    for tr in toremove:
        if tr in event_keys1:
            event_keys1.remove(tr)


    listeQ = [22,23,24,25,26,15,
          72,20,21,160,154,108,
          113,117,120,122,133,9,
          28,121,136,137,138,153,
          214,215,217,228,249,250,
          252,253,254]
    
    listeQ = [str(i) for i in listeQ]
    
    dict_ex1 = {}
    
    for k in event_keys1 + listeQ:
        dict_ex1[k] = []
        
        
    for e in root1[0]:
        for qi in listeQ:
            dict_ex1[qi].append("")
            for q in e:
                q_id = q.attrib["qualifier_id"]
                if q_id == qi:
                    if "value" in q.attrib:
                        dict_ex1[q_id][-1] = q.attrib["value"]
                    else:
                        dict_ex1[q_id][-1] = 1

        for k in event_keys1:
            if k in e.attrib:
                dict_ex1[k].append(e.attrib[k])
            else:
                dict_ex1[k].append("")
                
    df_ex = pd.DataFrame(dict_ex1)
    
    return df_ex, event_keys1, listeQ





def ex_to_vec(df_ex, event_keys1, listeQ, count_team = True):
    df_ex = pd.DataFrame(df_ex).iloc[:-10,]

    keep_event = [1,2,3,4,5,6,7,8,9, 
                10,11,12,13,14,15,16,17,24,
                30,32,34,35,37,38,39,40,41,
                42,44,45,49,50,51,52,53,54,
                55,56,59,60,61,63,66,72,74] 
    keep_event = [ str(i) for i in keep_event]

    keys_to_keep = event_keys1

    count = {}
    dict_player = {}
    
    tid = df_ex["team_id"].unique()
    
    for col in event_keys1:
        
        if col == 'type_id':
            for ev in keep_event:
                idx1 =  (df_ex['outcome'] == "1")&(df_ex[col] == ev)
                idx0 =  (df_ex['outcome'] == "0")&(df_ex[col] == ev)

                count[ev + "_out_1"] = [np.sum(idx1)]
                count[ev + "_out_0"] = [np.sum(idx0)]
            
        elif col == "team_id":
            """
            if count_team == True:
                idx0 = (df_ex[col] == tid[0])
                idx1 = (df_ex[col] == tid[1])
                
                count['team_id_0'] = [np.sum(idx0)]
                count['team_id_1'] = [np.sum(idx1)]
            else:
                count['team_id_0'] = [-1]
                count['team_id_1'] = [-1]
            """
        elif col == "min":
            count[col] = pd.to_numeric(df_ex["min"]).mean()
        
        
        
    df_count = pd.DataFrame(count)

    for q in listeQ:
        tmp = df_ex[q].replace("", -1)
        if np.sum(tmp != -1) != 0:
            df_count["mean_q_"+q] = pd.to_numeric(tmp[tmp != -1]).mean()
        else:
            df_count["mean_q_"+q] = -1
    
    df_count["game_id"] = [df_ex["id"]]
    
    return df_count


def extract_player_data(df,event_keys1 ,listeQ ):
    
    liste_player = df["player_id"].unique()
    liste_player = df[df["player_id"]==1]
    if '' in liste_player:
        liste_player = np.delete(liste_player, liste_player == '')
    
    df_player = pd.DataFrame(columns = ["" for i in range(N)])
    e = 0
    for pl in [1]:
        idx = df["player_id"] == str(pl)
        df_red = df[idx]
        df_red = ex_to_vec(df_red, event_keys1, listeQ, False)
        df_red["player_id"] = pl
        df_red["player_team"] = df["team_id"][idx].iloc[0]
        if e == 0:
            df_player.columns = df_red.columns
            df_player.loc[e] = df_red.loc[0]

        else:
            df_player.loc[e] = df_red.loc[0] 
        e += 1
    return df_player



def process_data_for_team_player(df_equipe, df_player):
    df_equipe = df_equipe.fillna(-1)
    df_player = df_player.fillna(-1)

    tid = [ 139,  140,  143,  144,  145,  146,  147,  148,  149,  150,  152,  427,  428,  429,
      430,  694, 1028, 1395, 2128, 2130]

    df_equipe = df_equipe.sample(frac=1)

    X = df_equipe.drop(columns=['game_id'])
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(X)
    X = pd.DataFrame(x_scaled)
    n_exp, p = X.shape
    n_tid = len(tid)    

    Xp = df_player.drop(columns=['game_id', 'player_id', 
                                 'player_team'
                                ])
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(Xp)
    Xp = pd.DataFrame(x_scaled)

    return X, Xp


def predict_player(df_equipe, df_player):
    X, Xp =  process_data_for_team_player(df_equipe, df_player)
    with open("./model/model_eq_4_[500, 200, 150, 50]_acc_0.75_architecture.json", 'r') as f:
        model_eq = model_from_json(f.read())

    model_eq.load_weights("./model/model_eq_4_[500, 200, 150, 50]_acc_0.75_weights.h5")

    pred_team = model_eq.predict(X)
    
    pred_team = np.argmax(pred_team)

    with open("./model/model_pl_t"+str(pred_team)+"_architecture.json", 'r') as f:
        model_pl = model_from_json(f.read())

    model_pl.load_weights("./model/model_pl_t"+str(pred_team)+"_weights.h5")

    pred_player = model_pl.predict(Xp)
    pred_player = np.argmax(pred_player)


    output_file = open('pid.json').read()
    output_json = json.loads(output_file)

    return int(output_json["t"+str(pred_team)][pred_player])


def Result(xml_file):
    root1 = xml_file.getroot()
    df_last = lasttestevent(xml_file)
    prediteam,predix,prediy = predict_next(df_last)
    #predictions of the next event






    #team added 
    event_keys1 = root1[0][10].attrib.keys()
    toremove = ['version', 'timestamp','last_modified']
    for tr in toremove:
        if tr in event_keys1:
            event_keys1.remove(tr)

    listeQ = [22,23,24,25,26,15,
          72,20,21,160,154,108,
          113,117,120,122,133,9,
          28,121,136,137,138,153,
          214,215,217,228,249,250,
          252,253,254]
    
    listeQ = [str(i) for i in listeQ]
    dt, ek, lq = xml_to_csv(xml_file)

    df_player = extract_player_data(dt,ek,lq)
    pl_team = df_player["player_team"]
    dt_red_eq = dt[dt["team_id"] == pl_team.loc[0]]

    df_equipe = ex_to_vec(dt_red_eq, ek, lq)

    

    pred_player = predict_player(df_equipe, df_player)

    rendu = [pred_player,prediteam,prediy,predix]
    np.savetxt("res_psgx.csv", rendu, delimiter=";",header='')

    return None

Result(ET.parse("exemlpe.xml"))