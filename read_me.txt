Readme du projet agorize polytechnique, et le psg.

Librairies:
tensorflow
pandas
xml.etree.ElementTree
keras
sklearn
warning
json

Preparation des bases d'entrainement:

On a réalisé 3 bases d'entrainement:
Une pour trouver le club du joueur 1
Une pour trouver le joueur 1
Une pour trouver les positions, et la prochaine equipe.

Pour les deux premieres BDD on a réalisé une sélection d'event, et d'attributs
et on a sommé les données en fonction de la valeur souhaitée.
On a aussi ajouté des informations comme la minute.

Dans l'autre cas nous avons récupéré les 10 dernieres lignes, pour ensuite les transformer en colonne
avec les informations sur la position, l'event et l'equipe.
 
Pour composer les bases d'apprentissage, on a récupéré les données de 50 echantillons pour le premiers cas
puis dans le second cas on a récupéré  60 echantillons (1 par minute à partir de 15 minutes)
