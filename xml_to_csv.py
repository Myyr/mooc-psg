
# coding: utf-8




import xml.etree.ElementTree as ET
import pandas as pd

import os





#transforme les données d'un match en xml en csv. 
# - récupère les informations présentent dans un fichier après modification
# - sélection des qualifiers (voir doc taha)
def xml_to_csv(xml_file):
    tree1 = ET.parse(xml_file)
    root1 = tree1.getroot()
    
    event_keys1 = root1[0][0].attrib.keys()
    event_keys1.remove('version')
    event_keys1.remove('timestamp')
    event_keys1.remove('last_modified')
    event_keys1.remove('id')
    
    listeQ = [22,23,24,25,26,15,
          72,20,21,160,154,108,
          113,117,120,122,133,9,
          28,121,136,137,138,153,
          214,215,217,228,249,250,
          252,253,254]
    
    listeQ = [str(i) for i in listeQ]
    
    dict_ex1 = {}
    for k in event_keys1 + listeQ:
        dict_ex1[k] = []
        
    for e in root1[0]:
        for qi in listeQ:
            dict_ex1[qi].append("")
            for q in e:
                q_id = q.attrib["qualifier_id"]
                if q_id == qi:
                    if "value" in q.attrib:
                        dict_ex1[q_id][-1] = q.attrib["value"]
                    else:
                        dict_ex1[q_id][-1] = 1

        for k in event_keys1:
            if k in e.attrib:
                dict_ex1[k].append(e.attrib[k])
            else:
                dict_ex1[k].append("")
                
    df_ex = pd.DataFrame(dict_ex1)
    
    return df_ex





# df = xml_to_csv("./train_xml/f24-24-2016-853142-eventdetails.xml")




# df.head()

