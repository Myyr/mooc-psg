
# coding: utf-8

# In[83]:


import pandas as pd
import numpy as np


# In[84]:


df = pd.read_csv("./data/pred_pos_team_50.csv")


# In[85]:


print df.columns
print df.shape
df.head()


# In[86]:


team_ids = []
team_id_col = []
for i in range(1,11):
    team_ids += df["team_id_" + str(i)].unique().tolist()
    team_id_col += ["team_id_" + str(i)]
team_ids = np.unique(np.array(team_ids))


# In[87]:


print team_ids


# In[88]:


df = df.drop(columns=['Unnamed: 0'])


# In[89]:


df.head()


# In[90]:


df.isnull().values


# In[91]:


df = df.drop(df[df.isnull().values].index, axis = 0)
print df.isnull().values.any()


# In[92]:



for i in range(1,12):
    
    df["team_id_" + str(i)] .unique().tolist()
team_ids = np.unique(np.array(team_ids))


# In[93]:


df_cat = df.copy()
for i in range(1,12):
    df_cat["type_id_"+str(i)] = df_cat["type_id_"+str(i)].astype('category')
    df_cat["team_id_"+str(i)] = df_cat["team_id_"+str(i)].astype('category')



# In[94]:


df_cat["type_id_11"].head()


# In[95]:


df["type_id_11"].head()


# In[96]:


df_cat = df_cat.sample(frac=1)
X = df_cat.drop(columns=['team_id_11', 'type_id_11', 'x_11', 'y_11'])
Y = df_cat[['team_id_11', 'x_11', 'y_11']]


# In[97]:


print X.shape
print Y.shape

n_exp, p = X.shape


# In[98]:


n_train = int(0.75*n_exp)


X_train = X[:n_train]
Y_train = Y[:n_train]

X_test = X[n_train:]
Y_test = Y[n_train:]


# In[99]:


from sklearn.neural_network import MLPClassifier


# In[105]:


hl_size = 200
hl_nb = 2
clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(hl_size, hl_nb), random_state=1)


# In[110]:


clf.fit(X_train, Y_train['team_id_11']) 


# In[111]:


Y_pred = clf.predict(X_test)


# In[112]:


prec = np.sum(Y_pred == Y_test['team_id_11'])/float(Y_pred.shape[0])
print prec


# In[113]:


prec = np.sum(X_test['team_id_10'] == Y_test['team_id_11'])/float(Y_test.shape[0])
print prec


# In[110]:




